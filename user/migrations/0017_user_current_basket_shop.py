# Generated by Django 3.1.5 on 2022-08-30 16:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0017_auto_20211207_1338'),
        ('user', '0016_auto_20211209_0059'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='current_basket_shop',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='shop.shop'),
        ),
    ]
