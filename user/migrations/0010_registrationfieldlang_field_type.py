# Generated by Django 3.1.5 on 2021-12-06 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0009_auto_20211206_1817'),
    ]

    operations = [
        migrations.AddField(
            model_name='registrationfieldlang',
            name='field_type',
            field=models.CharField(choices=[('text', 'text'), ('textarea', 'textarea')], default='text', max_length=255),
        ),
    ]
