from django.contrib import admin
from .models import User, RegistrationField, RegistrationFieldLang, UserRegistrationField


class RegistrationFieldLangInline(admin.TabularInline):
    model = RegistrationFieldLang
    extra = 0


@admin.register(RegistrationField)
class RegistrationFieldAdmin(admin.ModelAdmin):
    inlines = [RegistrationFieldLangInline]


class UserRegistrationFieldInline(admin.TabularInline):
    model = UserRegistrationField
    extra = 0


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'email', 'name', 'surname', 'phone']
    search_fields = ['email', 'phone', 'surname', 'name']
    list_display_links = ['id', 'email']
    readonly_fields = ['cart', 'password', 'code']
    inlines = [UserRegistrationFieldInline]
