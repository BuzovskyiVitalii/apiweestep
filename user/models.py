from django.db import models
from django.utils.translation import gettext_lazy as _


class User(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=36, blank=True, null=True)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=40)
    name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    cart = models.TextField(blank=True, null=True)
    last_cart_activity = models.DateField(blank=True, null=True)
    permissions = models.IntegerField(default=3)
    phone = models.CharField(max_length=255, db_index=True)
    reg_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    last_visit = models.DateField(blank=True, null=True)
    exported = models.BooleanField(default=0)
    manager_id = models.IntegerField(default=0)
    export_orders = models.IntegerField(blank=True, null=True)
    sms_code = models.CharField(max_length=255)
    verified = models.SmallIntegerField(default=0)
    sub_status = models.IntegerField(default=1)
    id_code = models.ForeignKey('phone_code.PhoneCode', models.CASCADE, db_column='id_code')
    current_basket_shop = models.ForeignKey('shop.Shop', models.CASCADE, null=True)

    class Meta:
        managed = True
        db_table = 'users'


FIELD_TYPE_CHOICES = [
    ('text', 'text'),
    ('textarea', 'textarea')
]


class RegistrationField(models.Model):
    name = models.CharField(
        verbose_name='name',
        max_length=255,
        null=True,
        unique=True,
        help_text=_('Name attribute of a form field')
    )
    field_type = models.CharField(
        verbose_name=_('Field type'),
        max_length=255,
        choices=FIELD_TYPE_CHOICES,
        default='text'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Registration field')
        verbose_name_plural = _('Registration fields')


class RegistrationFieldLang(models.Model):
    registrationfield = models.ForeignKey(RegistrationField, on_delete=models.CASCADE)
    lang = models.ForeignKey('lang.Lang', verbose_name=_('Language'), on_delete=models.CASCADE)
    registrationfield_label = models.CharField(
        verbose_name=_('Label for registration field'),
        max_length=190,
        null=True,
    )

    def __str__(self):
        return self.lang.alias_lang

    class Meta:
        verbose_name = _('Translation for registration field')
        verbose_name_plural = _('Translations for registration field')
        unique_together = (('registrationfield', 'lang'),)


class UserRegistrationField(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    registrationfield = models.ForeignKey(RegistrationField, on_delete=models.CASCADE)
    value_text = models.TextField(max_length=254, null=True)

    class Meta:
        unique_together = (('registrationfield', 'user'),)

