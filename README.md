## Установка проекта на FASTVPS
##### Установка Google Chrome
1. Авторизуемся под рутом. Переходим в папку /home/
2. Скачиваем хром
    ```
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    ```
3. Устанавливаем хром
   ```
   apt install ./google-chrome-stable_current_amd64.deb
   ```
##### Установка Chrome driver
1. На странице https://chromedriver.chromium.org/ жмем на ссылку 
Latest stable release: ChromeDriver 88.0.4324.96. Попадаем на страницу доступных загрузок. Копируем ссылку для linix64.
Скачиваем хром
   ```
   wget https://chromedriver.storage.googleapis.com/89.0.4389.23/chromedriver_linux64.zip
   ```
2. Разархивируем chromedriver_linux64.zip
   ```
   unzip chromedriver_linux64.zip
   ```
3. Move chromedriver to /usr/bin directory and change permissions...
   ```
   sudo mv chromedriver /usr/bin/chromedriver
   sudo chown root:root /usr/bin/chromedriver
   sudo chmod +x /usr/bin/chromedriver
   ```
##### Установка Python 3.9
1. Обновляем репозитарии
   ```
    sudo apt update && sudo apt upgrade
   ```
2. Then install the required packages for the compilation of Python source code
   ```
   sudo apt install wget build-essential libreadline-gplv2-dev libncursesw5-dev \
        libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev libffi-dev zlib1g-dev
   ```
   For Postgresql:
   ```
   sudo apt install libpq-dev
   ```
3. Переходим по ссылке https://www.python.org/downloads/source/
4. Копируем ссылку на Gzipped source tarball: https://www.python.org/ftp/python/3.9.2/Python-3.9.2.tgz
5. В переходим в home и скачиваем python 
   ```
   wget https://www.python.org/ftp/python/3.9.2/Python-3.9.2.tgz
   ```
6.	Распаковываем архив <br>
    ```
    tar xzf Python-3.9.2.tgz
    ```
7. Compiling Python Source – Change to the extracted directory with cd command, 
then prepare the Python source code for the compilation on your system.
   ```
   cd Python-3.9.2 
   ./configure --enable-optimizations 
   ```
8.	Install Python 3.9 – Finally, run the following command to complete the Python installation on Debian system. 
    The altinstall prevents the compiler to override default Python versions <br>
    `````
    make altinstall
    `````
<br>Wait for the Python installation complete on your system
##### Установка сайта
1. Под пользователем weestepuser авторизуемя в FASTPANEL
2. Создаем сайт apitest.weestep.pl. PHP отключаем. Базу данных не создаем. Зону DNS не создаем.
3. В директории сайта удаляем index.php.
4. Карточка сайта/настройки/основные - удаляем псевдоним www.apitest.weestep.pl
5. Устанавливаем сертификат lets encrypt. Убеждаемся, что сайт доступен по https.
6. Устанавливаем git от рута: 
   `````
   apt install git
   `````
7. Помещаем пользователя в sudoers: usermod -aG sudo weestepuser
8. Переходим в директорию проекта
   ````
   cd /var/www/weestepuser/data/www/apitest.weestep.pl/
   ````
9. Переключаемся на пользователя weestepuser
   ````
   su weestepuser
   ````
10. Удаляем содержимое директории
    ````
    rm -rf *
    ````
11. Создаем виртуальное окружение
    ````
    python3.9 -m venv env
    ````
12. Активируем виртуальное окружение
    `````
    source env/bin/activate
    `````
13. Копируем проект из репозитария в папку project
    `````
    git clone https://BuzovskyiVitalii@bitbucket.org/BuzovskyiVitalii/apiweestep.git ./project
    `````
14. Указываем хранить данные постоянно
    `````
    git config credential.helper store
    `````
15. Обновляем pip
    ````
    python -m pip install --upgrade pip
    ````
16. Переходим в папку project с файлом manage.py. Создаем папки media и static
    ````
    mkdir media static
    ````
17. Устанавливаем зависимости
    `````
    pip install -r requirements.txt
    `````
18. Запускаем проект:
    ````
    python manage.py runserver 185.4.74.193:8000
    ````
    или
    ````
    python manage.py runserver apitest.weestep.pl:8000
    ````
19. Завершаем работу сервера джанго. Устанавливаем gunicorn
    ````
        pip install gunicorn
    ````
20. Проверяем работу сайта через gunicorn
    ````
    gunicorn --workers=3 --bind 185.4.74.193:8000 project.wsgi
    ````
21. Убеждаемся, что сайт работает. Останавливаем gunicorn. Переходим в директорию:
    ````
    cd /etc/systemd/system
    ````
22. Редактируем файл
    ````
    sudo vim apitest.weestep.pl.gunicorn.service
    ````
    ````
    [Unit]
    Description=apitest.weestep.pl gunicorn service
    After=network.target
    [Service]
    WorkingDirectory=/var/www/weestepuser/data/www/apitest.weestep.pl/project
    ExecStart=/var/www/weestepuser/data/www/apitest.weestep.pl/env/bin/gunicorn --access-logfile - \
        --workers 3 \
        --bind unix:/run/apitest.weestep.pl.gunicorn.sock \
        --time 120 \
        project.wsgi:application
    [Install]
    WantedBy=multi-user.target
    ````
23. Создаем файл
    ````
    sudo vim apitest.weestep.pl.gunicorn.socket
    ````
    ````
    [Unit]
    Description=apitest.weestep.pl gunicorn socket
    [Socket]
    ListenStream=/run/apitest.weestep.pl.gunicorn.sock
    [Install]
    WantedBy=sockets.target
    ````
24. Запускаем и активируем сокет
    ````
    sudo systemctl start apitest.weestep.pl.gunicorn.socket
    sudo systemctl enable apitest.weestep.pl.gunicorn.socket
    ````
25. Проверяем наличие файла
    ````
    file /run/apitest.weestep.pl.gunicorn.sock
    ````
26. Проверим соединение через CURL
    ````
    curl --unix-socket /run/apitest.weestep.pl.gunicorn.sock localhost
    ````
27. И снова запросим вывод статуса
    ````
    sudo systemctl status apitest.weestep.pl.gunicorn.service
    ````
28. И перезапускаем процессы Gunicorn
    ````
    sudo systemctl daemon-reload
    sudo systemctl restart apitest.weestep.pl.gunicorn.service
    ````
##### Настройка NGINX
1. Переходим в директорию
   ````
   su root
   cd /etc/nginx/fastpanel2-available/weestepuser/
   ````
2. Редактируем файл
   ````
   vim apitest.weestep.pl.conf
   ````
   Добавляем директивы для статики и медиа
   ````
   location /static/ {
       alias /var/www/weestepuser/data/www/apitest.weestep.pl/project/static/;
   }

   location /media/ {
       alias /var/www/weestepuser/data/www/apitest.weestep.pl/project/media/;    
   }
   ````
   Редиктируем директиву location /. Замечание: при редактировании конфигов nginx через fastpanel
   панель по какой-то причине выдавала ошибку. При удалении строки **include proxy_params;** ошибка пропадает
   ````
   location / {
           include /etc/nginx/proxy_params;
           proxy_pass http://unix:/run/apitest.weestep.pl.gunicorn.sock;
           proxy_connect_timeout 3600s;
   	       proxy_send_timeout 3600s;
   	       proxy_read_timeout 3600s;
   }
   ````
   Комментируем директиву
   ````
   #    location ~* ^.+\.(jpg|jpeg|gif|png|svg|js|css|mp3|ogg|mpeg|avi|zip|gz|bz2|rar|swf|ico|7z|doc|docx|map|ogg|otf|pdf|tff|tif|txt|wav|webp|woff|woff2|xls|xlsx|xml)$ {
   #       try_files $uri $uri/ =404;
   #    }
   ````
3. Протестируем на наличие ошибок в синтаксисе:
   ````
   sudo nginx -t
   ````
4. Если ошибок нет, то перезапускаем сервер
   ````
   sudo systemctl restart nginx
   ````
**Замечание по поводу сертификата ssl lets encrypt**
К сожалению, после ручных правок в виртуальном хосту, панель не может корректно работать с ним. 
Сертификат, к сожалению, не может быть выпущен и установлен на такой виртуальный хост автоматически.
Сейчас Вы можете скопировать Вашу рабочую конфигурацию, затем восстановить хост на штатные настройки панели, 
установить сертификат и затем внести Ваши изменения в конфигурацию, где уже будет сертификат.
Пожалуйста, обратите внимание, что сертификат такой выдается всего на 3 месяца и его нужно будет продлять 
в таком ручном режиме.


##### Как работает чат
1. Пользователь заходит на сайт и получает session_id
2. Пользователь пишет сообщение в чат.
	
   
	

 