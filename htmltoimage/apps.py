from django.apps import AppConfig


class HtmltoimageConfig(AppConfig):
    name = 'htmltoimage'
