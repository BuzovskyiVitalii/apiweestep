from django.urls import path

from . import views


urlpatterns = [
    path('htmltoimage/', views.HtmlToImageView.as_view()),
]
