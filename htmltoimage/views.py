import os
import time
from wsgiref.util import FileWrapper

from django.http import HttpResponse
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView

import time
from PIL import Image
from selenium import webdriver
import base64
from selenium.webdriver.chrome.options import Options


class HtmlToImageView(APIView):

    def post(self, request):
        page_url = request.data.get('page_url', False)

        image = f'{time.time()}.png'
        image_path = os.path.join(settings.MEDIA_ROOT, image)

        # options = webdriver.ChromeOptions()
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        driver = webdriver.Chrome(options=chrome_options)

        driver.get(page_url)
        #
        S = lambda X: driver.execute_script('return document.body.parentNode.scroll' + X)
        window_width = request.data.get('window_width', S('Width'))
        window_height = request.data.get('window_height', S('Height'))
        driver.set_window_size(window_width, window_height)  # May need manual adjustment
        zoom_factor = int(request.data.get('body_zoom', 100)) / 100
        if 'body_zoom' in request.data:
            driver.execute_script(f"document.body.style.zoom='{request.data['body_zoom']}%'")

        # Делаем скриншот всей страницы
        driver.get_screenshot_as_file(image_path)

        if 'class_name' in request.data:
            element = driver.find_elements_by_class_name(request.data['class_name'])[0]
            left = int(element.location['x']) * zoom_factor
            top = int(element.location['y']) * zoom_factor
            right = int(element.location['x'] + element.size['width']) * zoom_factor
            bottom = int(element.location['y'] + element.size['height']) * zoom_factor
            im = Image.open(image_path)
            im = im.crop((left, top, right, bottom))
            im.save(image_path)

        with open(image_path, "rb") as image_file:
            image_64_str = base64.b64encode(image_file.read())
        # print(encoded_string)
        os.remove(image_path)

        # # imgdata = base64.b64decode(el.screenshot_as_base64)
        # # filename = 'some_image.jpg'  # I assume you have a way of picking unique filenames
        # # with open(filename, 'wb') as f:
        # #     f.write(imgdata)
        # # el.screenshot('web_screenshot.png')
        # # driver.find_element_by_class_name('big-banner-wrapper').screenshot('web_screenshot.png')
        driver.quit()
        return Response(image_64_str)
        # return Response('')
        # path = request.META['HTTP_HOST']
        # path1 = "http://" + path + settings.MEDIA_URL
        # url = path1 + image
        # return Response({'file': url})

