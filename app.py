import time
from selenium import webdriver
from PIL import Image
import base64
from selenium.webdriver.chrome.options import Options


options = webdriver.ChromeOptions()
options.headless = True
driver = webdriver.Chrome(options=options)

URL = 'https://weestep.pl/'

driver.get(URL)
time.sleep(4)
#driver.save_screenshot("screenshot.png")

S = lambda X: driver.execute_script('return document.body.parentNode.scroll'+X)
driver.set_window_size(S('Width'),S('Height')) # May need manual adjustment
driver.set_window_size(1600, S('Height')) # May need manual adjustment
# # el = driver.find_element_by_tag_name('body')
# el = driver.find_elements_by_class_name('search-button')[0]
element = driver.find_element_by_id('navbar')
driver.get_screenshot_as_file('screenshot.png')
left = int(element.location['x'])
top = int(element.location['y'])
right = int(element.location['x'] + element.size['width'])
bottom = int(element.location['y'] + element.size['height'])
print (left, top, right, bottom)
im = Image.open('screenshot.png')
im = im.crop((left, top, right, bottom))
im.save('new_screenshot.png')


# el = driver.find_element_by_id('navbar')
# el.screenshot("screenshot.png")
#
# imgdata = base64.b64decode(el.screenshot_as_base64)
#
# filename = 'some_image.jpg'  # I assume you have a way of picking unique filenames
# with open(filename, 'wb') as f:
#     f.write(imgdata)
# el.screenshot('web_screenshot.png')
# driver.find_element_by_class_name('big-banner-wrapper').screenshot('web_screenshot.png')

driver.quit()
