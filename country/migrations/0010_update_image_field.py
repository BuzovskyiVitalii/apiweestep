from django.db import migrations
from country.models import Country


def forwards(apps, schema_editor):
    countries_list = Country.objects.all()
    for c in countries_list:
        if str(c.image_country):
            c.image_country = 'category/flag/' + str(c.image_country)
            c.save()


class Migration(migrations.Migration):

    dependencies = [
        ('country', '0009_auto_20211115_1914'),
    ]

    operations = [
        # migrations.RunPython(forwards),
    ]
