from django.db import connection, migrations, models
from django.db.utils import OperationalError
from pprint import pprint, pformat

# Database name
referenced_table_schema = connection.settings_dict['NAME']
# Table for which primary key is added
table_name = "country_lang"
# Being added field with primary key
primary_field = "id_country_lang"


def forwards_prepare(apps, schema_editor):
    cursor = connection.cursor()

    try:
        # 1. Add field
        cursor.execute(
            """
            ALTER TABLE {table} ADD {field} INT(11) NOT NULL FIRST;
            """.format(table=table_name, field=primary_field))
    except OperationalError as e:
        if e.args[0] == 1060:  # Duplicate column name
            print(f"Error {e.args[0]}: {e.args[1]}. Continue...")
        else:
            exit(e)  # Exit if unexpected OperationalError

    # 2. Update field
    cursor.execute(
        """
            SET @num := 0;
            UPDATE {table_name} SET {primary_field} = @num := @num + 1;
        """.format(table_name=table_name, primary_field=primary_field)
    )
    cursor.close()


def forwards(apps, schema_editor):
    cursor = connection.cursor()
    # 3. Set primary key
    cursor.execute(
        """
        ALTER TABLE {table_name} ADD PRIMARY KEY({primary_field});
        ALTER TABLE {table_name} CHANGE {primary_field} {primary_field} INT(11) NOT NULL AUTO_INCREMENT;
        ALTER TABLE `country_lang` CHANGE `id_country` `id_country` INT(11) NULL DEFAULT NULL;
        ALTER TABLE `country_lang` CHANGE `id_lang` `id_lang` INT(11) NULL DEFAULT NULL;
        ALTER TABLE `country_lang` CHANGE `default_lang` `default_lang` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT '1-язык по умолчанию';
        """.format(table_name=table_name, primary_field=primary_field)
    )
    cursor.close()


def backwards(apps, schema_editor):
    # Drop field
    cursor = connection.cursor()
    cursor.execute(f"ALTER TABLE {table_name} DROP {primary_field}")
    cursor.close()


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('country', '0005_auto_20211006_1704'),
    ]

    operations = [
        # migrations.RunPython(forwards_prepare, reverse_code=migrations.RunPython.noop),
        # migrations.RunPython(forwards, reverse_code=backwards),
    ]
