from django.db import migrations
from country.models import Country


def forwards(apps, schema_editor):
    countries_list = Country.objects.all()
    for c in countries_list:
        if str(c.image_country):
            c.image_country = str(c.image_country).replace("category", "country")
            c.save()


class Migration(migrations.Migration):

    dependencies = [
        ('country', '0011_auto_20211122_1920'),
    ]

    operations = [
        # migrations.RunPython(forwards),
    ]