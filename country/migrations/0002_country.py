# Generated by Django 3.1.5 on 2021-10-06 11:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('country', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id_country', models.AutoField(primary_key=True, serialize=False)),
                ('country', models.CharField(max_length=255, unique=True, verbose_name='Country')),
                ('image_country', models.CharField(blank=True, max_length=255, null=True, verbose_name='Flag')),
                ('alpha2code', models.CharField(blank=True, max_length=255, null=True, unique=True)),
            ],
            options={
                'verbose_name': 'Country',
                'verbose_name_plural': 'Countries',
                'db_table': 'country',
                'managed': True,
            },
        ),
    ]
