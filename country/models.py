# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
import os
from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from lang.models import Lang
from django.utils.html import mark_safe


class Country(models.Model):
    id_country = models.AutoField(primary_key=True)
    country = models.CharField(
        _('Country'),
        unique=True,
        max_length=255,
        help_text=_('Country name in English, for example Ukraine'),
    )
    image_country = models.ImageField(_('Flag'), upload_to='country/flag', null=True)
    alpha2code = models.CharField(
        unique=True,
        max_length=255,
        blank=True,
        null=True,
        help_text=_('Two-letter designation, <a href="https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes" '
                    'target="_blank"> alpha-2 code column</a>, for example UA')
    )

    @property
    def country_flag_preview(self):
        if self.image_country:
            return mark_safe('<img src="{url}" width="20px" />'.format(url=self.image_country.url))
        return ""

    def __str__(self):
        return self.country

    class Meta:
        managed = True
        db_table = 'country'
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')


class CountryLang(models.Model):
    default_language_choices = [
        (1, _('Yes')),
        (0, _('No')),
    ]
    id_country_lang = models.AutoField(primary_key=True)
    id_country = models.ForeignKey(Country, on_delete=models.CASCADE, db_column='id_country', null=True)
    id_lang = models.ForeignKey(
        Lang,
        verbose_name=_('Language'),
        on_delete=models.CASCADE,
        db_column='id_lang',
        null=True)
    default_lang = models.SmallIntegerField(
        verbose_name=_('Default lang'),
        choices=default_language_choices,
        default=0,
    )

    class Meta:
        managed = True
        db_table = 'country_lang'
        unique_together = (('id_country', 'id_lang'),)
        verbose_name = _('The language of a country')
        verbose_name_plural = _('The languages of a country')


@receiver(post_delete, sender=Country)
def post_delete_image_country(sender, instance, *args, **kwargs):
    """When we delete Country instance, delete old image file """
    try:
        instance.image_country.delete(save=False)
    except:
        pass


@receiver(pre_save, sender=Country)
def pre_save_image_country(sender, instance, *args, **kwargs):
    """When update Country, delete old image file.Instance old image file will delete from os """
    try:
        old_img = instance.__class__.objects.get(id_country=instance.id_country).image_country.path
        try:
            new_img = instance.image_country.path
        except:
            new_img = None
        if new_img != old_img:
            if os.path.exists(old_img):
                os.remove(old_img)
    except:
        pass
