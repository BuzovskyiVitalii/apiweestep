from django.contrib import admin
from .models import Country, CountryLang
from django.utils.translation import gettext_lazy as _


class CountryLangInline(admin.TabularInline):
    model = CountryLang
    extra = 0


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('id_country', 'country', 'alpha2code', 'country_flag_preview')
    list_display_links = list_display
    inlines = [CountryLangInline]
    readonly_fields = ('country_flag_preview',)

    def country_flag_preview(self, obj):
        return obj.country_flag_preview

    country_flag_preview.short_description = _('Country flag')
    country_flag_preview.allow_tags = True

