from django.db import models
from lang.models import Lang


class Advantages(models.Model):
    id_advantage = models.AutoField(primary_key=True)
    priority = models.IntegerField()

    objects = models.Manager

    class Meta:
        managed = True
        db_table = 'advantages'


class AdvantagesLang(models.Model):
    id_advantage = models.ForeignKey(Advantages, on_delete=models.CASCADE, db_column='id_advantage')
    id_lang = models.ForeignKey(Lang, on_delete=models.CASCADE, db_column='id_lang')
    title = models.TextField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    objects = models.Manager

    class Meta:
        managed = True
        db_table = 'advantages_lang'
        unique_together = (('id_advantage', 'id_lang'),)
