from django.db import migrations
from django.db import connection


def forwards(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("""
            ALTER TABLE advantages CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        """)


def backwards(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("""
            ALTER TABLE advantages CONVERT TO CHARACTER SET latin1 COLLATE latin1_swedish_ci;
        """)


class Migration(migrations.Migration):

    dependencies = [
        ('advantage', '0003_auto_20230509_2253'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=backwards),
        # migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
