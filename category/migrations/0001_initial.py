from django.db import connection, migrations, models
from django.db.utils import OperationalError
from pprint import pprint, pformat

# Database name
referenced_table_schema = connection.settings_dict['NAME']
# Table for which primary key is added
table_name = "category_lang"
# Being added field with primary key
primary_field = "id_category_lang"
# Before removing old primary key, foreign keys should be removed
# Table being referenced by foreign keys of current table
referenced_table_name = "('lang', 'categories')"
# Field being referenced by current table
referenced_column_name = "('id_lang', 'id')"
# Column names having foreign keys
column_name = "('id_lang', 'id_category')"
# Unique fields
unique = "(`id_category`, `id_lang`)"


def forwards_prepare(apps, schema_editor):
    cursor = connection.cursor()

    try:
        # 1. Add field
        cursor.execute(
            """
            ALTER TABLE {table} ADD {field} INT(11) NOT NULL FIRST;
            """.format(table=table_name, field=primary_field))
    except OperationalError as e:
        if e.args[0] == 1060:  # Duplicate column name
            print(f"Error {e.args[0]}: {e.args[1]}. Continue...")
        else:
            exit(e)  # Exit if unexpected OperationalError

    # 2. get foreign keys
    cursor.execute(
        """
        SELECT
             TABLE_NAME
            ,COLUMN_NAME
            ,CONSTRAINT_NAME
            ,REFERENCED_TABLE_NAME
            ,REFERENCED_COLUMN_NAME
        FROM
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE
            REFERENCED_TABLE_SCHEMA = '{referenced_table_schema}' AND
            REFERENCED_TABLE_NAME IN {referenced_table_name} AND
            REFERENCED_COLUMN_NAME IN {referenced_column_name} AND
            TABLE_NAME = '{table_name}' AND
            COLUMN_NAME IN {column_name}
        """.format(
            referenced_table_schema=referenced_table_schema,
            referenced_table_name=referenced_table_name,
            referenced_column_name=referenced_column_name,
            table_name=table_name,
            column_name=column_name,
        )
    )

    columns = [col[0] for col in cursor.description]
    foreign_keys = [dict(zip(columns, row)) for row in cursor.fetchall()]

    # 3. Remove foreign keys
    for foreignkey in foreign_keys:
        cursor.execute(
            """
            ALTER TABLE {db}.{table} DROP FOREIGN KEY {fk}
            """.format(db=referenced_table_schema, table=foreignkey['TABLE_NAME'], fk=foreignkey['CONSTRAINT_NAME'])
        )

    # 4. Drop primary key
    try:
        cursor.execute(f"ALTER TABLE {table_name} DROP PRIMARY KEY")
    except OperationalError as e:
        if e.args[0] == 1091:  # "Can't DROP 'PRIMARY'; check that column/key exists"
            print(f"Error {e.args[0]}: {e.args[1]}. Continue...")
        else:
            exit(e)  # Exit if unexpected OperationalError

    # 5. Create FK for tables
    for foreignkey in foreign_keys:
        cursor.execute(
            """
            ALTER TABLE {db}.{table} ADD FOREIGN KEY ({column})
            REFERENCES {referenced_table}({referenced_column}) ON DELETE CASCADE ON UPDATE CASCADE;
            """.format(
                db=referenced_table_schema,
                table=foreignkey['TABLE_NAME'],
                column=foreignkey['COLUMN_NAME'],
                referenced_table=foreignkey['REFERENCED_TABLE_NAME'],
                referenced_column=foreignkey['REFERENCED_COLUMN_NAME'],
            )
        )

    # 6. Update field
    cursor.execute(
        """
            SET @num := 0;
            UPDATE {table_name} SET {primary_field} = @num := @num + 1;
        """.format(table_name=table_name, primary_field=primary_field)
    )
    cursor.close()


def forwards(apps, schema_editor):
    cursor = connection.cursor()
    # 7. Set primary key
    cursor.execute(
        """
        ALTER TABLE {table_name} ADD PRIMARY KEY({primary_field});
        ALTER TABLE {table_name} CHANGE {primary_field} {primary_field} INT(11) NOT NULL AUTO_INCREMENT;
        """.format(table_name=table_name, primary_field=primary_field)
    )
    cursor.close()


def backwards(apps, schema_editor):
    # Drop field
    cursor = connection.cursor()
    cursor.execute(f"ALTER TABLE {table_name} DROP {primary_field}")
    cursor.close()


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('lang', '0003_auto_20210803_1937'),
    ]

    operations = [
        # migrations.RunPython(forwards_prepare, reverse_code=migrations.RunPython.noop),
        # migrations.RunPython(forwards, reverse_code=backwards),
    ]
