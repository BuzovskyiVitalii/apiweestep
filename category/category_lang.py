# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class CategoryLang(models.Model):
    id_category_lang = models.PositiveIntegerField(primary_key=True)
    id_category = models.IntegerField()
    id_lang = models.IntegerField()
    title = models.CharField(max_length=128)
    description = models.TextField(blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    meta_title = models.CharField(max_length=255, blank=True, null=True)
    meta_keywords = models.CharField(max_length=255, blank=True, null=True)
    meta_description = models.CharField(max_length=512, blank=True, null=True)
    h1 = models.TextField(blank=True, null=True)
    var_category_product_h1 = models.TextField(blank=True, null=True)
    product_appearance_date = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category_lang'
        unique_together = (('id_category', 'id_lang'),)
