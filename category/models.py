# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
import os
from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

from lang.models import Lang
from django.core.validators import FileExtensionValidator
from django.utils.translation import gettext_lazy as _


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.CharField(max_length=255)
    image = models.CharField(max_length=255, blank=True, null=True)
    icon = models.FileField(upload_to="category/icons", validators=[FileExtensionValidator(['svg'])], blank=True, null=True)
    seen = models.PositiveIntegerField()
    priority = models.IntegerField()
    code = models.CharField(unique=True, max_length=36, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'categories'
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        try:
            category_lang = CategoryLang.objects.filter(id_lang=Lang.objects.current_id_lang, id_category=self.pk).get()
            return category_lang.title
        except CategoryLang.DoesNotExist:
            return self.url


class CategoryLang(models.Model):
    id_category_lang = models.AutoField(primary_key=True)
    id_category = models.ForeignKey(Category, on_delete=models.CASCADE, db_column="id_category", null=True)
    id_lang = models.ForeignKey(Lang, on_delete=models.CASCADE, db_column="id_lang", null=True)
    title = models.CharField(max_length=129)
    description = models.TextField(blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)
    meta_title = models.CharField(max_length=255, blank=True, null=True)
    meta_keywords = models.CharField(max_length=255, blank=True, null=True)
    meta_description = models.CharField(max_length=512, blank=True, null=True)
    h1 = models.TextField(blank=True, null=True)
    var_category_product_h1 = models.TextField(blank=True, null=True)
    product_appearance_date = models.CharField(max_length=250, blank=True, null=True)

    objects = models.Manager()

    class Meta:
        managed = True
        db_table = 'category_lang'
        unique_together = (('id_category', 'id_lang'),)
        verbose_name = _('Category_translation')
        verbose_name_plural = _('Categories_translations')


@receiver(post_delete, sender=Category)
def post_save_icon(sender, instance, *args, **kwargs):
    """When we delete Category instance, delete old icon file """
    try:
        instance.icon.delete(save=False)
    except:
        pass


@receiver(pre_save, sender=Category)
def pre_save_image(sender, instance, *args, **kwargs):
    """When update Category, delete old icon file.Instance old image file will delete from os """
    try:
        old_img = instance.__class__.objects.get(id=instance.id).icon.path
        try:
            new_img = instance.icon.path
        except:
            new_img = None
        if new_img != old_img:
            if os.path.exists(old_img):
                os.remove(old_img)
    except:
        pass

