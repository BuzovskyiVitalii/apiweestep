from django.contrib import admin

from .models import Category
from .models import CategoryLang


class CategoryLangInline(admin.TabularInline):
    model = CategoryLang
    extra = 0


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    inlines = [CategoryLangInline]


admin.site.site_title = "Category"
admin.site.site_header = "Category"
