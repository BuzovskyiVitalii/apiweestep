from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from shop.models import Shop


class ArchiveProductsForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['shop'].choices = [(shop.id_shop, shop.shop) for shop in Shop.objects.all()]

    file = forms.FileField(required=True)
    shop = forms.MultipleChoiceField(choices=[])

    def clean_file(self):
        data = self.cleaned_data['file']
        if data.content_type == 'application/vnd.ms-excel':
            raise ValidationError(
                _('Invalid file type: %(value)s. Convert file into xlsx type'),
                code='invalid',
                params={'value': data.content_type},
            )
        file_ext = data.name.split('.')[-1]
        if file_ext != 'xlsx':
            raise ValidationError(
                _('Invalid file extension: %(value)s. Convert file into xlsx type'),
                code='invalid',
                params={'value': file_ext},
            )
        max_file_size = 10*1024*1024  # 20 MB
        if int(data.size) > max_file_size:
            raise ValidationError(
                _('Your file size is %(file_size)s KB and it exceeds maximum file size %(max_file_size)s KB'),
                code='invalid',
                params={'file_size': round(int(data.size) / 1024), 'max_file_size': round(max_file_size / 1024)},
            )
        return data
