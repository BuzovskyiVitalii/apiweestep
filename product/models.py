from django.db import models
from django.utils.translation import gettext_lazy as _


class Products(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=36)
    modification = models.CharField(max_length=36, blank=True, null=True)
    category = models.IntegerField(blank=True, null=True)
    article = models.CharField(max_length=255, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    in_stock = models.IntegerField()
    when_appear = models.CharField(max_length=255, blank=True, null=True)
    image = models.CharField(max_length=255, blank=True, null=True)
    image_1 = models.CharField(max_length=255, blank=True, null=True)
    image_2 = models.CharField(max_length=255, blank=True, null=True)
    image_3 = models.CharField(max_length=255, blank=True, null=True)
    image_4 = models.CharField(max_length=255, blank=True, null=True)
    inner_material = models.CharField(max_length=255, blank=True, null=True)
    outer_material = models.CharField(max_length=255, blank=True, null=True)
    meta_title = models.CharField(max_length=255, blank=True, null=True)
    meta_description = models.CharField(max_length=255, blank=True, null=True)
    meta_keywords = models.CharField(max_length=255, blank=True, null=True)
    priority = models.IntegerField(default=0)
    sex = models.IntegerField(blank=True, null=True)
    pairs_included = models.CharField(max_length=255, blank=True, null=True)
    min_size = models.IntegerField(blank=True, null=True)
    max_size = models.IntegerField(blank=True, null=True)
    set_sizes = models.CharField(max_length=36, blank=True, null=True)
    repeats = models.CharField(max_length=255, blank=True, null=True)
    qty = models.IntegerField(blank=True, null=True)
    archived = models.IntegerField()
    discount = models.IntegerField()
    orthopedist = models.IntegerField()
    exchange = models.IntegerField()
    creation_date = models.DateTimeField(_('The date of product creation'), blank=True, null=True)
    shops = models.ManyToManyField('shop.Shop', through='ProductShop')

    def __str__(self):
        return self.article

    class Meta:
        managed = True
        db_table = 'products'
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class ProductShop(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    shop = models.ForeignKey('shop.Shop', on_delete=models.CASCADE)
    archived = models.IntegerField(
        choices=((0, _('Not archived')), (1, _('Archived')))
    )
    do_not_change_archived = models.BooleanField(
        verbose_name=_('Do not change archived'),
        default=False,
        help_text=_('If active exchange with 1c does not affect field archived')
    )

    class Meta:
        unique_together = [['product', 'shop']]

    def __str__(self):
        return f"{self.shop.shop} - {self.product.article}"


class CreationDate(Products):
    class Meta:
        proxy = True
        verbose_name = _('The date of product creation')
        verbose_name_plural = _('The dates of product creation')


class Image360(models.Model):
    product = models.OneToOneField(Products, verbose_name=_('Vendor code'), on_delete=models.CASCADE)
    url = models.CharField(_('Image 360 url'), max_length=255)
    updated = models.DateTimeField(verbose_name=_('Updated'), auto_now=True, editable=False)

    class Meta:
        verbose_name = _('Image 360')
        verbose_name_plural = _('Images 360')


class ProductArchived(Products):
    class Meta:
        proxy = True
        verbose_name = _('Hide products')
        verbose_name_plural = _('Hide products')
