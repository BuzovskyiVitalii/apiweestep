from django.core.management.base import BaseCommand
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.conf import settings
import urllib.request
import json
from product.models import Products, Image360


class Command(BaseCommand):
    """
    The programm for mapping product and remote image 360 url
    """
    help = 'The programm for mapping product and remote image 360 url'

    def __init__(self):
        super(Command, self).__init__()

    def handle(self, *args, **options):
        headers = {'APIKEY': settings.IMAGES_360_API_KEY}
        req = urllib.request.Request(settings.IMAGES_360_HOST, headers=headers)
        responce = urllib.request.urlopen(req).read()
        requested_images360_list = json.loads(responce)
        if not len(requested_images360_list):
            return False
        vendor_codes_list = []
        vendor_codes_urls_dict = {}
        # Get the list of product articles
        for image_360_dict in requested_images360_list:
            vendor_codes_list.append(image_360_dict['vendor_code'])
            vendor_codes_urls_dict[image_360_dict['vendor_code']] = image_360_dict['image360url']
        # Remove all images which does not exist on remote storage
        Image360.objects.select_related('product').exclude(product__article__in=vendor_codes_list).delete()

        for product in Products.objects.filter(article__in=vendor_codes_list).all():
            if vendor_codes_urls_dict[product.article]:
                Image360.objects.update_or_create(
                    product=product,
                    defaults={'product': product, 'url': vendor_codes_urls_dict[product.article]},
                )

        # Check if remote iframe.html are accessible
        for image360info in Image360.objects.all():
            try:
                responce = urllib.request.urlopen(image360info.url)
                if not (responce.code in [200, 301, 302, 303, 307, 308]):
                    image360info.delete()
            except:
                image360info.delete()

        return True

