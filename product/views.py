import decouple
from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse, HttpResponseForbidden
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from .management.commands import import_images360
from .serializers import ProductListByArticlesSerializer
from .models import Products
from rest_framework.parsers import JSONParser


class Images360(APIView):
    """
    http://127.0.0.1:8000/api/v1/product/get-remote-images360
    Get the urls of images 360 from the remote storage
    """
    def get(self, request):
        import_images360.Command().handle()
        return Response('done')


class ProductsListByArticles(APIView):
    """
    http://127.0.0.1:8000/api/v1/product/get-products-list-by-articles/
    Get products list by articles
    """

    def post(self, request):
        """
        Example of json in request:
        {"api_key": "11111111111111111", "articles":["R178637782 BK", "R178637782 BKK"]}
        Get all products: {"api_key": "11111111111111111", "articles":[]}
        """
        products = Products.objects.all()

        if len(request.data) == 0 or settings.SITE_API_KEY != request.data.get('api_key'):
            return HttpResponseForbidden()
        elif request.data.get('articles') is None:
            products = products.none()
        elif not isinstance(request.data.get('articles'), list):
            products = products.none()
        elif len(request.data.get('articles')) == 0:
            pass
        else:
            products = products.filter(article__in=request.data.get('articles'))

        serializer = ProductListByArticlesSerializer(products, many=True)
        return JsonResponse(serializer.data, safe=False)
