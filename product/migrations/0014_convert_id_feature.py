from django.db import connection
from django.db import migrations, models

# Database name
referenced_table_schema = connection.settings_dict['NAME']
# Table being referenced by foreign keys of another tables
referenced_table_name = "feature"
# Field being referenced by another tables
referenced_column_name = "id_feature"


def forwards(apps, schema_editor):
    with connection.cursor() as cursor:
        # 1. Get tables and fields, whose FK reference field referenced_column_name of the table referenced_table_name
        cursor.execute(
            """
            SELECT
                 TABLE_NAME
                ,COLUMN_NAME
                ,CONSTRAINT_NAME
                ,REFERENCED_TABLE_NAME
                ,REFERENCED_COLUMN_NAME
            FROM
                INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE
                REFERENCED_TABLE_SCHEMA = %s AND
                REFERENCED_TABLE_NAME = %s AND
                REFERENCED_COLUMN_NAME = %s
            """, [referenced_table_schema, referenced_table_name, referenced_column_name])
        columns = [col[0] for col in cursor.description]
        result = [dict(zip(columns, row)) for row in cursor.fetchall()]
        for foreignkey in result:
            # 2. Remove FK of the tables, which reference the field referenced_column_name of table referenced_table_name
            cursor.execute(
                """
                ALTER TABLE {db}.{table} DROP FOREIGN KEY {fk}
                """.format(db=referenced_table_schema, table=foreignkey['TABLE_NAME'], fk=foreignkey['CONSTRAINT_NAME'])
            )
            # 3. Once the FK is removed the function of field changing is available
            cursor.execute(
                """
                ALTER TABLE {db}.{table} CHANGE {column} {column} INT(11) NOT NULL;
                """.format(db=referenced_table_schema, table=foreignkey['TABLE_NAME'], column=foreignkey['COLUMN_NAME'])
            )
        # 4. Change field, that foreign keys are referenced
        cursor.execute(
            """
            ALTER TABLE {db}.{table} CHANGE {column} {column} INT(11) NOT NULL AUTO_INCREMENT;
            """.format(db=referenced_table_schema, table=referenced_table_name, column=referenced_column_name)
        )
        # 5. Create FK for tables
        for foreignkey in result:
            cursor.execute(
                """
                ALTER TABLE {db}.{table} ADD FOREIGN KEY ({column})
                REFERENCES {referenced_table}({referenced_column}) ON DELETE CASCADE ON UPDATE CASCADE;
                """.format(
                    db=referenced_table_schema,
                    table=foreignkey['TABLE_NAME'],
                    column=foreignkey['COLUMN_NAME'],
                    referenced_table=referenced_table_name,
                    referenced_column=referenced_column_name
                )
            )

    print('finish')


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0013_auto_20220823_1430'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=migrations.RunPython.noop),
    ]
