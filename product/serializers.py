from rest_framework import serializers
from product.models import Products


class ProductListByArticlesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['id', 'article']
