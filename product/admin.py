from io import BytesIO
from openpyxl import load_workbook

from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import path, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.utils.html import mark_safe
from django.contrib import messages
from django.template.response import SimpleTemplateResponse
from django.core.exceptions import ObjectDoesNotExist

from .models import Products, CreationDate, Image360, ProductShop, ProductArchived
from shop.models import Shop
from .management.commands import import_images360
from .admin_form import ArchiveProductsForm


class ProductShopInline(admin.TabularInline):
    model = ProductShop
    extra = 0


@admin.register(Products)
class ProductAdmin(admin.ModelAdmin):
    inlines = [ProductShopInline]


@admin.register(CreationDate)
class CreationDateAdmin(admin.ModelAdmin):
    list_display = ['id', 'article', 'creation_date']
    list_editable = ['creation_date']
    search_fields = ['article']
    actions = None
    list_display_links = None

    def has_add_permission(self, request):
        return False


@admin.register(Image360)
class Image360Admin(admin.ModelAdmin):
    list_display = ['id', 'product_id', 'product', 'updated', 'image360url']
    list_display_links = ['product_id', 'product']
    fields = ['product_id', 'product', 'url', 'updated', 'image360']
    readonly_fields = ['product_id', 'image360', 'updated']

    def product_id(self, obj):
        return obj.product_id
    product_id.short_description = _('Product ID')

    def image360url(self, obj):
        return mark_safe(f'<a href="{obj.url}">{obj.url}</a>')
    image360url.short_description = _('Image 360 url')

    def image360(self, obj):
        context = {'url': obj.url}
        content = SimpleTemplateResponse('admin/product/image360/iframe.html', context)
        return mark_safe(content.render().content.decode("utf-8"))
    image360.short_description = _('Image 360')

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-images360/', self.process_import, name="import_images360"),
        ]
        return my_urls + urls

    def process_import(self, request):
        result = import_images360.Command().handle()
        if result:
            messages.success(request, _('Images 360 imported successfully'))
        else:
            messages.warning(request, _('Images 360 were not imported'))
        return HttpResponseRedirect("../")


def remove_from_archived(modeladmin, request, queryset):
    """Action that removes products from archived"""
    for product in queryset:
        for product_shop in product.productshop_set.all():
            product_shop.do_not_change_archived = False
            product_shop.archived = 0
            product_shop.save()


remove_from_archived.short_description = _('Remove products from archived')


@admin.register(ProductArchived)
class ProductArchivedAdmin(admin.ModelAdmin):
    inlines = [ProductShopInline]
    actions = [remove_from_archived]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        if request.method == 'POST' and request.POST.get('archive_products_form'):
            archive_products_form = ArchiveProductsForm(request.POST, request.FILES)
            if archive_products_form.is_valid():
                # process the data in form.cleaned_data as required
                handle_uploaded_file(request, archive_products_form)
                messages.success(request, _('Products archived successfully'))
                # redirect to a new URL:
                return HttpResponseRedirect(reverse_lazy('admin:product_productarchived_changelist'))

        # if a GET (or any other method) we'll create a blank form
        else:
            archive_products_form = ArchiveProductsForm()
        extra_context['archive_products_form'] = archive_products_form
        return super().changelist_view(
            request, extra_context=extra_context,
        )

    def get_queryset(self, request):
        queryset = super(ProductArchivedAdmin, self).get_queryset(request)
        return queryset.filter(productshop__do_not_change_archived=True).distinct()


def handle_uploaded_file(request, form):
    f = request.FILES['file']
    wb = load_workbook(filename=BytesIO(f.read()))
    ws = wb.active
    for row in range(1, ws.max_row + 1):
        try:
            product = Products.objects.filter(article=ws.cell(row=row, column=1).value).get()
            for id_shop in form.cleaned_data['shop']:
                shop = Shop.objects.filter(id_shop=id_shop).get()
                ProductShop.objects.update_or_create(
                    product=product, shop=shop,
                    defaults={'archived': 1, 'do_not_change_archived': True}
                )
        except ObjectDoesNotExist:
            continue


    # print(ws.max_row)
