from django.urls import path

from . import views


urlpatterns = [
    path('product/get-remote-images360/', views.Images360.as_view()),
    path('product/get-products-list-by-articles/', views.ProductsListByArticles.as_view())
]
