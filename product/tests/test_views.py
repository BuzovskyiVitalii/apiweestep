# from django.test import SimpleTestCase
# from ..serializers import ProductListByArticlesSerializer
# from ..models import Products
#
#
# class TestCaseProductApi(SimpleTestCase):
#
#     def test_get_products_list_by_articles(self):
#         """
#         Run test: python manage.py test product.tests.test_views.TestCaseProductApi.test_get_products_list_by_articles
#         """
#         resp = self.client.post('/api/v1/product/get-products-list-by-articles/')
#         self.assertEqual(resp.status_code, 200)
#
#     def test_product_list_by_article_serializer(self):
#         """
#         python manage.py test product.tests.test_views.TestCaseProductApi.test_product_list_by_article_serializer
#         """
#         product = Products.objects.first()
#         serializer = ProductListByArticlesSerializer(product)
#         self.assertEqual(product.id, serializer.data['id'])
#         self.assertEqual(product.article, serializer.data['article'])
