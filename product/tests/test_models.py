from django.test import TestCase
from product.models import Products


class CatalogueTestCase(TestCase):
    fixtures = ['dump.json']  # fixture with no user models

    def test_something(self):
        product = Products.objects.filter(article='R178637783 DB').get()
        print(product.id)
        self.assertEqual('R178637783 DB', product.article)
