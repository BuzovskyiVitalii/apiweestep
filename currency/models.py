from django.db import models
from django.utils.translation import gettext_lazy as _

ISO_4217_LINK = '<a target="_blank" href="https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D1%81' \
                '%D1%83%D1%89%D0%B5%D1%81%D1%82%D0%B2%D1%83%D1%8E%D1%89%D0%B8%D1%85_%D0%B2%D0%B0%D0%BB%D1%8E%D1%82' \
                '">ISO 4217</a> '


class Currency(models.Model):
    id_currency = models.AutoField(primary_key=True)
    name_iso_4217 = models.CharField(
        verbose_name=_('Currency title'),
        max_length=255,
        help_text="{}, {}".format(_('Currency title, for example US Dollar'), ISO_4217_LINK)
    )
    name_rus = models.CharField(
        verbose_name=_('Currency title in russian'),
        max_length=255,
        help_text="{} Доллар США".format(_('Currency title in russian, for example')),
    )
    code_iso_4217 = models.CharField(
        verbose_name=_('Currency code'),
        unique=True,
        max_length=255,
        help_text="{} USD, {}".format(_('Currency code, for example'), ISO_4217_LINK)
    )
    sign = models.CharField(
        verbose_name=_('Currency designation'),
        max_length=255,
        help_text="{} $, {}".format(_('Currency designation, for example'), ISO_4217_LINK)
    )
    exchange_rate = models.FloatField(
        verbose_name=_('Exchange rate'),
        help_text=_('Exchange rate against currency from 1c')
    )
    decimal_places = models.PositiveIntegerField(
        verbose_name=_('Decimal places'),
        help_text=_('Decimal places')
    )
    currency_1c = models.BooleanField(
        default=False,
        help_text=_('Currency 1c')
    )

    def __str__(self):
        return self.name_iso_4217

    class Meta:
        managed = True
        db_table = 'currency'
        verbose_name = _('Currency')
        verbose_name_plural = _('Currencies')


class ShopCurrency(models.Model):
    id_shop_currency = models.AutoField(primary_key=True)
    id_shop = models.ForeignKey('shop.Shop', on_delete=models.CASCADE, db_column='id_shop', null=True)
    id_currency = models.ForeignKey(
        Currency,
        verbose_name=_('Currency'),
        on_delete=models.CASCADE, db_column='id_currency', null=True)

    def __str__(self):
        return f"{self.id_shop.shop} -> {self.id_currency.name_iso_4217}"

    class Meta:
        managed = True
        db_table = 'shop_currency'
        unique_together = (('id_shop', 'id_currency'),)
        verbose_name = _('Shop currency')
        verbose_name_plural = _('Shop currencies')
