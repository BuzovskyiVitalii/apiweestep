from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class PhoneCodeConfig(AppConfig):
    name = 'phone_code'
    verbose_name = _('Phone code')
