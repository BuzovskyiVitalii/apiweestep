# Generated by Django 3.1.5 on 2021-12-04 15:07

from django.db import migrations, models


def reorder(apps, schema_editor):
    PhoneCode = apps.get_model("phone_code", "PhoneCode")
    order = 0
    for item in PhoneCode.objects.all():
        order += 1
        item.priority = order
        item.save()


class Migration(migrations.Migration):

    dependencies = [
        ('phone_code', '0005_auto_20211204_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonecode',
            name='priority',
            field=models.PositiveIntegerField(default=0, verbose_name='Position'),
        ),
        # migrations.RunPython(reorder),
    ]
