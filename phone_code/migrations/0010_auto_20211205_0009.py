# Generated by Django 3.1.5 on 2021-12-04 22:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_code', '0009_auto_20211205_0005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonecode',
            name='length',
            field=models.SmallIntegerField(help_text='The number of digits, for example +380980000000 - 12 digits', null=True, verbose_name='Quantity of numbers'),
        ),
    ]
