# Generated by Django 3.1.5 on 2021-12-03 22:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('phone_code', '0002_phonecode'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='phonecode',
            options={'managed': True},
        ),
    ]
