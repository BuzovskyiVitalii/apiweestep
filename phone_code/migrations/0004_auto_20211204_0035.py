# Generated by Django 3.1.5 on 2021-12-03 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_code', '0003_auto_20211204_0034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonecode',
            name='active',
            field=models.BooleanField(default=1),
        ),
    ]
