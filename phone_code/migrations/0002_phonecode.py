# Generated by Django 3.1.5 on 2021-12-03 22:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('lang', '0007_auto_20211116_1237'),
        ('phone_code', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PhoneCode',
            fields=[
                ('id_code', models.AutoField(primary_key=True, serialize=False)),
                ('country', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('code', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('priority', models.PositiveIntegerField()),
                ('active', models.PositiveIntegerField()),
                ('length', models.PositiveIntegerField(blank=True, null=True)),
                ('id_lang', models.ForeignKey(db_column='id_lang', on_delete=django.db.models.deletion.CASCADE, to='lang.lang')),
            ],
            options={
                'db_table': 'phone_code',
                'managed': True,
            },
        ),
    ]
