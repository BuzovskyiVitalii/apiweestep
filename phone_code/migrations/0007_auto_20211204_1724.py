# Generated by Django 3.1.5 on 2021-12-04 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone_code', '0006_auto_20211204_1707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonecode',
            name='priority',
            field=models.PositiveIntegerField(default=0, verbose_name='position control'),
        ),
    ]
