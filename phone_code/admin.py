from django.contrib import admin
from .models import PhoneCode
from django.db.models import Count
from adminsortable2.admin import SortableAdminMixin
from django.utils.translation import gettext_lazy as _


@admin.register(PhoneCode)
class PhoneCodeAdmin(SortableAdminMixin, admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('id_lang')
        qs = qs.annotate(users_count=Count('user'))
        return qs
    list_display = ('id_code', 'country', 'code', 'priority', 'priority_num', 'active', 'length', 'lang_for_mailing',
                    'get_users_count')
    list_display_links = ('id_code', 'country')
    list_editable = ('active',)

    def lang_for_mailing(self, obj):
        return obj.id_lang.lang

    lang_for_mailing.short_description = _('Language for mailing')

    def priority_num(self, obj):
        return obj.priority

    def get_users_count(self, obj):
        return obj.users_count

    priority_num.short_description = _('Position')
    get_users_count.short_description = _('Users count')
