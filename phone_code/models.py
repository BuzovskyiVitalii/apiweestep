from django.db import models
from django.utils.translation import gettext_lazy as _


class PhoneCode(models.Model):
    id_code = models.AutoField(verbose_name='id_code', primary_key=True)
    country = models.CharField(
        verbose_name=_('Country'), unique=True, max_length=255, blank=False, null=True)
    code = models.SmallIntegerField(
        verbose_name=_('International phone code'),
        unique=True,
        null=True,
        help_text=_('Country dialing code, for example: 380')
    )
    priority = models.PositiveIntegerField(
        verbose_name=_('Position control'),
        default=0,
        blank=False,
        null=False,
    )
    active = models.BooleanField(verbose_name=_('Active'), default=1)
    length = models.SmallIntegerField(
        verbose_name=_('Quantity of numbers'),
        null=True,
        help_text=_('The number of digits, for example +380980000000 - 12 digits')
    )
    id_lang = models.ForeignKey(
        'lang.Lang',
        verbose_name=_('The language of mailing'),
        on_delete=models.CASCADE,
        db_column='id_lang',
        help_text=_('Bind the language that will be used when sending letters to users to the telephone country code')
    )

    def __str__(self):
        return f"{self.country} -> {self.code}"

    class Meta:
        """
        python manage.py reorder phone_code.PhoneCode
        """
        managed = True
        db_table = 'phone_code'
        ordering = ['priority']
        verbose_name = _('Phone code')
        verbose_name_plural = _('Phone codes')

