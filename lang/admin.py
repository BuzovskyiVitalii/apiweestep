from django.contrib import admin
from .models import Lang


@admin.register(Lang)
class LangAdmin(admin.ModelAdmin):
    list_display = ('lang', 'id_lang', 'iso_code', 'alias_lang', 'do_not_translate')
    list_editable = ('do_not_translate',)

# Register your models here.
