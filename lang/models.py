from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _

do_not_translate_choices = [
    (1, _('Doesn\'t participate in translations')),
    (0, _('Participates in translations')),
]


class LangManager(models.Manager):
    @property
    def current_id_lang(self):
        """
        Property that contains the id_lang used to display content in admin panel
        :return: (integer) id_lang
        """
        return self.filter(iso_code=settings.MY_ADMIN_ISO_CODE).get().id_lang


class Lang(models.Model):
    id_lang = models.AutoField(primary_key=True)
    lang = models.CharField(
        unique=True,
        max_length=255,
        help_text=_('The native name of the language, for example Українська, column'
                    ' <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes" target="_blank">Native name</a>')
    )
    iso_code = models.CharField(
        unique=True,
        max_length=255,
        help_text=_('Two-letter designation, for example uk, column <a target="_blank" '
                    'href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">639-1</a>')
    )
    alias_lang = models.CharField(
        unique=True,
        max_length=255,
        help_text=_('English title, e.g. ukrainian, column <a target="_blank" '
                    'href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO language name</a>')
    )
    do_not_translate = models.PositiveIntegerField(_('Participation in translations'), choices=do_not_translate_choices)

    objects = LangManager()

    def __str__(self):
        return self.alias_lang

    class Meta:
        managed = True
        db_table = 'lang'
        verbose_name = _('Language')
        verbose_name_plural = _('Languages')
