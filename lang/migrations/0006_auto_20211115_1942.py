# Generated by Django 3.1.5 on 2021-11-15 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lang', '0005_auto_20211115_1919'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lang',
            name='alias_lang',
            field=models.CharField(help_text='English title, e.g. ukrainian, column <a target="_blank" href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO language name</a>', max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='lang',
            name='do_not_translate',
            field=models.PositiveIntegerField(choices=[(1, "Doesn't participate in translations"), (0, 'Participates in translations')], verbose_name='Participation in translations'),
        ),
    ]
