# Generated by Django 3.1.5 on 2021-12-06 12:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0014_shopregistrationfield'),
    ]

    operations = [
        migrations.CreateModel(
            name='ShopRegistrationFieldProxy',
            fields=[
            ],
            options={
                'verbose_name': 'Shop registration field',
                'verbose_name_plural': 'Shop registration fields',
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('shop.shop',),
        ),
    ]
