from django.db import models
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from category.models import Category
from country.models import Country
from lang.models import Lang


class Shop(models.Model):
    id_shop = models.AutoField(primary_key=True)
    shop = models.CharField(
        verbose_name=_('Shop'),
        unique=True,
        max_length=255,
        help_text="{} lideropt.com.ua".format(_('Domain name, for example'))
    )
    # Название
    # домена, например
    class Meta:
        managed = True
        db_table = 'shop'
        verbose_name = _('Shop')
        verbose_name_plural = _('Shops')

    def __str__(self):
        return self.shop


class ShopPriceCoefficient(models.Model):
    category = models.ForeignKey(Category, verbose_name=_('Category'), on_delete=models.CASCADE, null=True)
    shop = models.ForeignKey(Shop, verbose_name=_('Shop'), on_delete=models.CASCADE, null=True)
    price_coefficient = models.FloatField(null=True)

    class Meta:
        unique_together = ('category', 'shop')


class ShopProxy(Shop):
    class Meta:
        proxy = True
        verbose_name = _('Price coefficient')
        verbose_name_plural = _('Price coefficients')

    def __str__(self):
        return self.shop


class ShopCountry(models.Model):
    id_shop_country = models.AutoField(primary_key=True)
    id_shop = models.OneToOneField(Shop, on_delete=models.CASCADE, db_column='id_shop', null=True)
    id_country = models.OneToOneField(
        Country,
        verbose_name=_('Country'),
        on_delete=models.CASCADE,
        db_column='id_country',
        null=True
    )

    class Meta:
        managed = True
        db_table = 'shop_country'
        unique_together = (('id_shop', 'id_country'),)
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return self.id_country.country


class ShopAdminLang(models.Model):
    default_language_choices = [
        (1, _('Yes')),
        (0, _('No')),
    ]
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    lang = models.ForeignKey(Lang, verbose_name=_('Language'), on_delete=models.CASCADE)
    default_lang = models.SmallIntegerField(
        verbose_name=_('Default Language in admin'),
        choices=default_language_choices,
        default=0,
    )

    def __str__(self):
        return f"{self.shop.shop} -> {self.lang.lang}"

    class Meta:
        verbose_name = _('Languages of admin panel')
        verbose_name_plural = _('Languages of admin panel')
        unique_together = (('shop', 'lang'),)


class ShopRegistrationField(models.Model):
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    registrationfield = models.ForeignKey(
        'user.RegistrationField',
        verbose_name=_('Shop registration field'),
        on_delete=models.CASCADE
    )
    required = models.BooleanField(_('Required field'), default=False)
    position = models.SmallIntegerField(_('Position'), default=0, blank=False, null=False)

    def __str__(self):
        return self.registrationfield.name

    class Meta:
        ordering = ['position']
        verbose_name = _('Shop registration field')
        verbose_name_plural = _('Shop registration fields')
        unique_together = (('shop', 'registrationfield'),)


class ShopRegistrationFieldProxy(Shop):
    class Meta:
        proxy = True
        verbose_name = _('Shop registration field')
        verbose_name_plural = _('Shop registration fields')

    def __str__(self):
        return self.shop








