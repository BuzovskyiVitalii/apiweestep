from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from .models import Shop, ShopPriceCoefficient, ShopProxy, ShopCountry, ShopAdminLang, ShopRegistrationFieldProxy, \
    ShopRegistrationField
from currency.models import ShopCurrency
from adminsortable2.admin import SortableInlineAdminMixin
from django.utils.translation import gettext_lazy as _


class ShopPriceCoefficientInline(admin.TabularInline):
    model = ShopPriceCoefficient
    extra = 0


class ShopCountryInline(admin.TabularInline):
    model = ShopCountry
    extra = 0


class ShopAdminLangInline(admin.TabularInline):
    model = ShopAdminLang
    extra = 0


class ShopCurrencyInline(admin.TabularInline):
    model = ShopCurrency
    extra = 0


@admin.register(Shop)
class ShopAdmin(admin.ModelAdmin):
    """Shop"""
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('shopcountry').select_related('shopcountry__id_country')
        return qs

    empty_value_display = '-'
    list_display = ('id', '__str__', 'country')
    list_display_links = ('__str__',)
    inlines = [ShopCountryInline, ShopAdminLangInline, ShopCurrencyInline]
    readonly_fields = ('id',)
    fields = (('shop', 'id'),)

    def country(self, obj):
        return obj.shopcountry.id_country.country

    def id(self, obj):
        return obj.id_shop

    country.short_description = _('Country of a shop')


@admin.register(ShopProxy)
class ShopProxyAdmin(admin.ModelAdmin):
    """Price coefficient"""
    readonly_fields = ("shop",)
    inlines = [ShopPriceCoefficientInline]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


class ShopRegistrationFieldInline(SortableInlineAdminMixin, admin.TabularInline):
    model = ShopRegistrationField
    extra = 0

    def get_position(self, obj):
        return obj.position

    readonly_fields = ('get_position',)
    get_position.short_description = _('Position')


@admin.register(ShopRegistrationFieldProxy)
class ShopRegistrationFieldProxyAdmin(admin.ModelAdmin):
    """Registration fields"""
    readonly_fields = ("shop",)
    inlines = (ShopRegistrationFieldInline,)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


admin.site.site_title = "Weestep"
admin.site.site_header = "Weestep"